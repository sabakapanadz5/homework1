// subject
let subject = [];
subject["javascript"] = [];
subject["react"] = [];
subject["python"] = [];
subject["java"] = [];
subject["javascript"]["subjectCredit"] = 4;
subject["react"]["subjectCredit"] = 7;
subject["python"]["subjectCredit"] = 6;
subject["java"]["subjectCredit"] = 3;

subject["creditsum"] =
  subject["javascript"]["subjectCredit"] +
  subject["react"]["subjectCredit"] +
  subject["python"]["subjectCredit"] +
  subject["java"]["subjectCredit"];

let student1 = [];
let student2 = [];
let student3 = [];
let student4 = [];
// student 1
student1["name"] = "Jan";
student1["surname"] = "Reno";
student1["age"] = 26;

student1["javascript"] = 62;
student1["react"] = 57;
student1["python"] = 88;
student1["java"] = 90;

let student1Sum =
  student1["javascript"] +
  student1["react"] +
  student1["python"] +
  student1["java"];
student1["sum"] = student1Sum;

let student1arthave = student1Sum / 4;
student1["artave"] = student1arthave;
//GPA
let student1gpa =
  (1 * subject["javascript"]["subjectCredit"] +
    0.5 * subject["react"]["subjectCredit"] +
    3 * subject["python"]["subjectCredit"] +
    3 * subject["java"]["subjectCredit"]) /
  subject["creditsum"];
student1["gpa"] = student1gpa;

console.log(student1["gpa"]);

//student 2
student2["name"] = "clod";
student2["surname"] = "mone";
student2["age"] = 19;

student2["javascript"] = 72;
student2["react"] = 52;
student2["python"] = 92;
student2["java"] = 67;

let student2Sum =
  student2["javascript"] +
  student2["react"] +
  student2["python"] +
  student2["java"];
student2["sum"] = student2Sum;

let student2arthave = student2Sum / 4;
student2["artave"] = student2arthave;
//GPA
let student2gpa =
  (2 * subject["javascript"]["subjectCredit"] +
    0.5 * subject["react"]["subjectCredit"] +
    4 * subject["python"]["subjectCredit"] +
    1 * subject["java"]["subjectCredit"]) /
  subject["creditsum"];

student2["gpa"] = student2gpa;
console.log(student2["gpa"]);

//student 3
student3["name"] = "van";
student3["surname"] = "gog";
student3["age"] = 26;

student3["javascript"] = 51;
student3["react"] = 98;
student3["python"] = 65;
student3["java"] = 70;

let student3Sum =
  student3["javascript"] +
  student3["react"] +
  student3["python"] +
  student3["java"];
student3["sum"] = student3Sum;

let student3arthave = student3Sum / 4;
student3["artave"] = student3arthave;
//GPA
let student3gpa =
  (0.5 * subject["javascript"]["subjectCredit"] +
    4 * subject["react"]["subjectCredit"] +
    1 * subject["python"]["subjectCredit"] +
    1 * subject["java"]["subjectCredit"]) /
  subject["creditsum"];
student3["gpa"] = student3gpa;
console.log(student3["gpa"]);

//student 4
student4["name"] = "dam";
student4["surname"] = "square";
student4["age"] = 36;

student4["javascript"] = 82;
student4["react"] = 53;
student4["python"] = 80;
student4["java"] = 65;

let student4Sum =
  student4["javascript"] +
  student4["react"] +
  student4["python"] +
  student4["java"];
student4["sum"] = student4Sum;

let student4arthave = student4Sum / 4;
student4["artave"] = student4arthave;

//GPA
let student4gpa =
  (3 * subject["javascript"]["subjectCredit"] +
    0.5 * subject["react"]["subjectCredit"] +
    2 * subject["python"]["subjectCredit"] +
    1 * subject["java"]["subjectCredit"]) /
  subject["creditsum"];

student4["gpa"] = student4gpa;

console.log(student4["gpa"]);

// best GPA
if (
  student1["gpa"] > student2["gpa"] &&
  student1["gpa"] > student3["gpa"] &&
  student1["gpa"] > student4["gpa"]
) {
  console.log(`${student1["name"]} has best gpa ${student1["gpa"]} `);
} else if (
  student2["gpa"] > student1["gpa"] &&
  student2["gpa"] > student3["gpa"] &&
  student2["gpa"] > student4["gpa"]
) {
  console.log(`${student2["name"]} has best gpa ${student2["gpa"]} `);
} else if (
  student3["gpa"] > student1["gpa"] &&
  student3["gpa"] > student2["gpa"] &&
  student3["gpa"] > student4["gpa"]
) {
  console.log(`${student3["name"]} has best gpa ${student3["gpa"]} `);
} else if (
  student4["gpa"] > student1["gpa"] &&
  student4["gpa"] > student2["gpa"] &&
  student4["gpa"] > student3["gpa"]
) {
  console.log(`${student4["name"]} has best gpa ${student4["gpa"]} `);
}

// best arithmetical avarage 21+
let bestPointsOver21 = "";
let over21 = false;

let maxValue = 0;

if (student1["age"] >= 21 && student1arthave > maxValue) {
  maxValue = student1arthave;
  bestPointsOver21 = "Jean Reno";
  over21 = true;
}

if (student2["age"] >= 21 && student2arthave > maxValue) {
  maxValue = student2arthave;
  bestPointsOver21 = "Claude Monet";
  over21 = true;
}

if (student3["age"] >= 21 && student3arthave > maxValue) {
  maxValue = student3arthave;
  bestPointsOver21 = "Van Gogh";
  over21 = true;
}

if (student4["age"] >= 21 && student4arthave > maxValue) {
  maxValue = student4arthave;
  bestPointsOver21 = "Dam Square";
  over21 = true;
}

console.log(`${bestPointsOver21} has best scores over 21`);

student1["frontEnd"] = student1["react"] + student1["javascript"];

student2["frontEnd"] = student2["react"] + student2["javascript"];

student3["frontEnd"] = student3["react"] + student3["javascript"];

student4["frontEnd"] = student4["react"] + student4["javascript"];

if (
  student1["frontEnd"] > student2["frontEnd"] &&
  student1["frontEnd"] > student3["frontEnd"] &&
  student1["frontEnd"] > student4["frontEnd"]
) {
  console.log(`${student1["name"]} has best front-end scores`);
} else if (
  student2["frontEnd"] > student1["frontEnd"] &&
  student2["frontEnd"] > student3["frontEnd"] &&
  student2["frontEnd"] > student4["frontEnd"]
) {
  console.log(`${student2["name"]} has best front-end scores`);
} else if (
  student3["frontEnd"] > student1["frontEnd"] &&
  student3["frontEnd"] > student2["frontEnd"] &&
  student3["frontEnd"] > student4["frontEnd"]
) {
  console.log(`${student3["name"]} has best front-end scores`);
} else if (
  student4["frontEnd"] > student1["frontEnd"] &&
  student4["frontEnd"] > student2["frontEnd"] &&
  student4["frontEnd"] > student3["frontEnd"]
) {
  console.log(`${student4["name"]} has best front-end scores`);
}

let aveSum =
  (student1arthave + student2arthave + student3arthave + student4arthave) / 4;

student1arthave > aveSum
  ? console.log(`${student1["name"]} has red diploma`)
  : console.log(`${student1["name"]} is noob`);
student2arthave > aveSum
  ? console.log(`${student2["name"]} has red diploma`)
  : console.log(`${student2["name"]} is noob`);
student3arthave > aveSum
  ? console.log(`${student3["name"]} has red diploma`)
  : console.log(`${student3["name"]} is noob `);
student4arthave > aveSum
  ? console.log(`${student4["name"]} has red diploma`)
  : console.log(`${student1["name"]} is noob`);
