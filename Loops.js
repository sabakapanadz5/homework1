let subjects = ["javascript", "react", "python", "java"];

// subject
let subjectCredit = {
  javascript: 4,
  react: 7,
  python: 6,
  java: 3,
};
subjectCredit.creditSum = 0;
for (let i = 0; i < subjects.length; i++) {
  subjectCredit.creditSum += subjectCredit[subjects[i]];
}
console.log(subjectCredit);

let students = [];
// student 1
students[0] = {
  name: "Jan",
  surname: "Reno",
  age: 26,
  scores: {
    javascript: 62,
    react: 57,
    python: 88,
    java: 90,
  },
};

//student 2
students[1] = {
  name: "Clod",
  surname: "Mone",
  age: 19,
  scores: {
    javascript: 77,
    react: 52,
    python: 92,
    java: 67,
  },
};

//student 3
students[2] = {
  name: "Van",
  surname: "Gog",
  age: 21,
  scores: {
    javascript: 51,
    react: 98,
    python: 65,
    java: 70,
  },
};

//student 4
students[3] = {
  name: "Dam",
  surname: "Square",
  age: 36,
  scores: {
    javascript: 82,
    react: 53,
    python: 80,
    java: 65,
  },
};
//sum
for (let i = 0; i < subjects.length; i++) {
  students[i].scores.sum = 0;
}

for (let i = 0; i < subjects.length; i++) {
  for (let j = 0; j < students.length; j++) {
    students[j].scores.sum += students[j].scores[subjects[i]];
  }
}

for (let i = 0; i < students.length; i++) {
  console.log(
    `${students[i].name} ${students[i].surname} has ${students[i].scores.sum}`
  );
}
// arth ave

for (let i = 0; i < subjects.length; i++) {
  students[i].scores.artAve = 0;
  students[i].scores.artAve += students[i].scores.sum / 4;
}
console.log(students);

//GPA
students[0].scores.Coefficient = [1, 0.5, 3, 3];
students[1].scores.Coefficient = [2, 0.5, 4, 1];
students[2].scores.Coefficient = [0.5, 4, 1, 1];
students[3].scores.Coefficient = [3, 0.5, 2, 1];

for (let i = 0; i < students.length; i++) {
  students[i].scores.GPA = 0;
  for (let j = 0; j < subjects.length; j++) {
    students[i].scores.GPA +=
      students[i].scores.Coefficient[j] * subjectCredit[subjects[j]];
  }
  students[i].scores.GPA /= subjectCredit.creditSum;
}
console.log(students);

// best GPA

let bestStudentName = students[0].name;
let bestStudentSurname = students[0].name;
let maxGPA = students[0].scores.GPA;

for (let i = 0; i < students.length; i++) {
  if (students[i].scores.GPA > maxGPA) {
    maxGPA = students[i].scores.GPA;
    bestStudentName = students[i].name;
    bestStudentSurname = students[i].surname;
  }
}
console.log(
  `${bestStudentName} ${bestStudentSurname} has the highest GPA! ${maxGPA}`
);

// best arithmetical avarage 21+
let bestPointsOver21 = "";
let maxValue = 0;

for (let i = 0; i < students.length; i++) {
  if (students[i].age >= 21 && students[i].scores.artAve > maxValue) {
    maxValue = students[i].scores.artAve;
    bestPointsOver21 = students[i].name;
  }
}
console.log(
  `${bestPointsOver21} has best arithmetical avarage 21+  ${maxValue} `
);

// front-End Scores
for (let i = 0; i < students.length; i++) {
  students[i].scores.frontEnd = 0;
  students[i].scores.frontEnd =
    students[i].scores.react + students[i].scores.javascript;
}

let bestFrontEndName = students[0].name;
let bestFrontEnd = students[0].scores.frontEnd;

for (let i = 0; i < students.length; i++) {
  if (students[i].scores.frontEnd > bestFrontEnd) {
    bestFrontEnd = students[i].scores.frontEnd;
    bestFrontEndName = students[i].name;
  }
}
console.log(`${bestFrontEndName}  is the best front-end developer!`);

// art ave sum
let aveSum = 0;
for (let i = 0; i < students.length; i++) {
  aveSum += students[i].scores.artAve;
}
aveSum /= 4;
console.log(aveSum);

for (let i = 0; i < students.length; i++) {
  students[i].scores.artAve > aveSum
    ? console.log(
        `${students[i].name} ${students[i].surname} got the Red Diploma!`
      )
    : console.log(`${students[i].name} ${students[i].surname} is in noob!`);
}
