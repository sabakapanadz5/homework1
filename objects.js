// subject
let subjectCredit = {
  javascriptCredit: 4,
  reactCredit: 7,
  pythonCredit: 6,
  javaCredit: 3,
};
subjectCredit.creditSum =
  subjectCredit.javascriptCredit +
  subjectCredit.pythonCredit +
  subjectCredit.reactCredit +
  subjectCredit.javaCredit;

let students = [];
// student 1
students[0] = {
  name: "Jan",
  surname: "Reno",
  age: 26,
  scores: {
    javascript: 62,
    react: 57,
    python: 88,
    java: 90,
  },
};

// sum

students[0].scores.sum =
  students[0].scores.javascript +
  students[0].scores.react +
  students[0].scores.python +
  students[0].scores.java;

// arithmetical average
students[0].scores.arithAve = students[0].scores.sum / 4;

//GPA
students[0].scores.gpa =
  (1 * subjectCredit.javascriptCredit +
    0.5 * subjectCredit.reactCredit +
    3 * subjectCredit.pythonCredit +
    3 * subjectCredit.javaCredit) /
  subjectCredit.creditSum;

console.log(students[0]);

//student 2
students[1] = {
  name: "Clod",
  surname: "Mone",
  age: 19,
  scores: {
    javascript: 77,
    react: 52,
    python: 92,
    java: 67,
  },
};

students[1].scores.sum =
  students[1].scores.javascript +
  students[1].scores.react +
  students[1].scores.python +
  students[1].scores.java;

// arithmetical average
students[1].scores.arithAve = students[1].scores.sum / 4;

//GPA
students[1].scores.gpa =
  (2 * subjectCredit.javascriptCredit +
    0.5 * subjectCredit.reactCredit +
    4 * subjectCredit.pythonCredit +
    1 * subjectCredit.javaCredit) /
  subjectCredit.creditSum;

//student 3
students[2] = {
  name: "Van",
  surname: "Gog",
  age: 26,
  scores: {
    javascript: 51,
    react: 98,
    python: 65,
    java: 70,
  },
};

students[2].scores.sum =
  students[2].scores.javascript +
  students[2].scores.react +
  students[2].scores.python +
  students[2].scores.java;

// arithmetical average
students[2].scores.arithAve = students[2].scores.sum / 4;

//GPA
students[2].scores.gpa =
  (0.5 * subjectCredit.javascriptCredit +
    4 * subjectCredit.reactCredit +
    1 * subjectCredit.pythonCredit +
    1 * subjectCredit.javaCredit) /
  subjectCredit.creditSum;

//student 4
students[3] = {
  name: "Dam",
  surname: "Square",
  age: 36,
  scores: {
    javascript: 82,
    react: 53,
    python: 80,
    java: 65,
  },
};
students[3].scores.sum =
  students[3].scores.javascript +
  students[3].scores.react +
  students[3].scores.python +
  students[3].scores.java;

// arithmetical average
students[3].scores.arithAve = students[3].scores.sum / 4;

//GPA
students[3].scores.gpa =
  (3 * subjectCredit.javascriptCredit +
    0.5 * subjectCredit.reactCredit +
    2 * subjectCredit.pythonCredit +
    1 * subjectCredit.javaCredit) /
  subjectCredit.creditSum;

console.log(students);

// best GPA
if (
  students[0].scores.gpa > students[1].scores.gpa &&
  students[0].scores.gpa > students[2].scores.gpa &&
  students[0].scores.gpa > students[3].scores.gpa
) {
  console.log(`${students[0].name} has best gpa ${students[0].scores.gpa} `);
} else if (
  students[1].scores.gpa > students[0].scores.gpa &&
  students[1].scores.gpa > students[2].scores.gpa &&
  students[1].scores.gpa > students[3].scores.gpa
) {
  console.log(`${students[1].name} has best gpa ${students[1].scores.gpa} `);
} else if (
  students[2].scores.gpa > students[0].scores.gpa &&
  students[2].scores.gpa > students[1].scores.gpa &&
  students[2].scores.gpa > students[3].scores.gpa
) {
  console.log(`${students[2].name} has best gpa ${students[2].scores.gpa} `);
} else if (
  students[3].scores.gpa > students[0].scores.gpa &&
  students[3].scores.gpa > students[1].scores.gpa &&
  students[3].scores.gpa > students[2].scores.gpa
) {
  console.log(`${students[3].name} has best gpa ${students[3].scores.gpa} `);
}

// best arithmetical avarage 21+
let bestPointsOver21 = "";
let over21 = false;
let maxValue = 0;

if (students[0].age >= 21 && students[0].scores.arithAve > maxValue) {
  maxValue = students[0].scores.arithAve;
  bestPointsOver21 = "Jean Reno";
  over21 = true;
}

if (students[1].age >= 21 && students[1].scores.arithAve > maxValue) {
  maxValue = students[1].scores.arithAve;
  bestPointsOver21 = "Claude Monet";
  over21 = true;
}

if (students[2].age >= 21 && students[2].scores.arithAve > maxValue) {
  maxValue = students[2].scores.arithAve;
  bestPointsOver21 = "Van Gogh";
  over21 = true;
}
if (students[3].age >= 21 && students[3].scores.arithAve > maxValue) {
  maxValue = students[3].scores.arithAve;
  bestPointsOver21 = "Dam Square";
  over21 = true;
}
if (!over21) {
  bestPointsOver21 = "No one is  21+";
}

console.log(`${bestPointsOver21} has best scores over 21`);

students[0].scores.frontEnd =
  students[0].scores.react + students[0].scores.javascript;
students[1].scores.fronEnd =
  students[1].scores.react + students[1].scores.javascript;
students[2].scores.frontEnd =
  students[2].scores.react + students[2].scores.javascript;
students[3].scores.fronEnd =
  students[3].scores.react + students[3].scores.javascript;

if (
  students[0].scores.frontEnd > students[1].scores.fronEnd &&
  students[0].scores.frontEnd > students[2].scores.frontEnd &&
  students[0].scores.frontEnd > students[3].scores.fronEnd
) {
  console.log(`${students[0].name} has best front-end scores`);
} else if (
  students[1].scores.fronEnd > students[0].scores.frontEnd &&
  students[1].scores.fronEnd > students[2].scores.frontEnd &&
  students[1].scores.fronEnd > students[3].scores.fronEnd
) {
  console.log(`${students[1].name} has best front-end scores`);
} else if (
  students[2].scores.frontEnd > students[0].scores.frontEnd &&
  students[2].scores.frontEnd > students[1].scores.fronEnd &&
  students[2].scores.frontEnd > students[3].scores.fronEnd
) {
  console.log(`${students[2].name} has best front-end scores`);
} else if (
  students[3].scores.fronEnd > students[0].scores.frontEnd &&
  students[3].scores.fronEnd > students[1].scores.fronEnd &&
  students[3].scores.fronEnd > students[2].scores.frontEnd
) {
  console.log(`${students[3].name} has best front-end scores`);
}

let aveSum =
  (students[0].scores.arithAve +
    students[1].scores.arithAve +
    students[2].scores.arithAve +
    students[3].scores.arithAve) /
  4;

students[0].scores.arithAve > aveSum
  ? console.log(`${students[0].name} has red diploma`)
  : console.log(`${students[0].name} is noob`);
students[1].scores.arithAve > aveSum
  ? console.log(`${students[1].name} has red diploma`)
  : console.log(`${students[1].name} is noob`);
students[2].scores.arithAve > aveSum
  ? console.log(`${students[2].name} has red diploma`)
  : console.log(`${students[2].name} is noob `);
students[3].scores.arithAve > aveSum
  ? console.log(`${students[3].name} has red diploma`)
  : console.log(`${students[3].name} is noob`);
